<html>
    <head>
        <title>Task1</title>
    </head>
    <body>
        <script>
            function isPrime(value) {
                var primes = [];
                for(var i = 2; i < 101; i++) {
                    primes[i] = true;
                }
                var limit = Math.sqrt(101);
                for(var i = 2; i < 101; i++) {
                    if(primes[i] === true) {
                        for(var j = i * i; j < 101; j += i) {
                            primes[j] = false;
                        }
                    }
                }
                for(var i = 2; i < 101; i++) {
                    if(primes[i] === true) {
                        document.write(i + " " + primes[i]);
                    }
                }
            }
        </script>

    </body>
</html>