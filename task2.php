<html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $( function() {
                $( "#datepicker" ).datepicker();
            } );
        </script>
    </head>
    <body>

        <div class="container">
            <h2>Task 2</h2>

            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open</button>


            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">


                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title">Upload Photo</h3>
                        </div>
                        <div class="modal-body">
                            <h2> Description</h2>
                            <form method="post" action="display.php" enctype="multipart/form-data">
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <div class="col-xs-4">
                                            <label >Description:</label>
                                        </div>
                                        <div class="col-xs-8 ">
                                            <textarea name ="description" type="text" class="form-control"></textarea>
                                        </div>

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <div class="col-xs-4">
                                            <label >Date:</label>
                                        </div>
                                        <div class="col-xs-8 ">
                                            <input name ="date" type="text" class="form-control" id="datepicker"></input>
                                        </div>

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <div class="col-xs-4">
                                            <label >Photo:</label>
                                        </div>
                                        <div class="col-xs-8 ">
                                            <input type="file" name="fileToUpload" id="fileToUpload" >
                                        </div>

                                    </div>
                                </div>

                                <input type="submit" value="Upload Image" name="submit" class="btn btn-success">

                            </form>



                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </body>
</html>